package com.mavrcorp;

import org.jetbrains.annotations.NotNull;
import org.junit.Test;

import java.util.Stack;

public class BraceSequenceTest {

    @Test
    public void positiveTest() {
        doTest("()");
        doTest("[({})]");
    }

    @Test(expected = AssertionError.class)
    public void negativeTest1() {
        doTest("[(])");
    }

    @Test(expected = AssertionError.class)
    public void negativeTest2() {
        doTest("[()");
    }

    private void doTest(@NotNull String str) {
        Stack<Character> stack = new Stack<>();
        for (char c : str.toCharArray()) {
            if (c == '(' || c == '{' || c == '[')
                stack.push(c);
            else if (c == ')' || c == '}' || c == ']') {
                Character t = stack.pop();
                boolean b = false;
                if (c == ')')
                    b = t == '(';
                else if (c == '}')
                    b = t == '{';
                else if (c == ']')
                    b = t == '[';
                assert b;
            }
        }
        assert stack.size() == 0;
    }
}
