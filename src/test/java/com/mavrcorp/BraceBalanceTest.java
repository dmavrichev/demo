package com.mavrcorp;

import org.jetbrains.annotations.NotNull;
import org.junit.Test;

public class BraceBalanceTest {

    @Test
    public void positiveTest() {
        checkBalance("()");
        checkBalance("(abc)");
        checkBalance("((o)(0))");
    }

    @Test(expected = AssertionError.class)
    public void negativeTest() {
        checkBalance(")(");
        checkBalance("(()");
    }

    private void checkBalance(@NotNull String string) {
        int balance = 0;
        for (char c : string.toCharArray()) {
            if (c == '(')
                balance++;
            else if (c == ')')
                balance--;
            else
                continue;
            assert balance >= 0;
        }
        assert balance == 0;
    }
}
