package com.mavrcorp.handler

import com.mavrcorp.domain.SessionHolder
import com.mavrcorp.domain.StompPrincipal
import org.slf4j.LoggerFactory
import org.springframework.http.server.ServerHttpRequest
import org.springframework.web.socket.WebSocketHandler
import org.springframework.web.socket.server.support.DefaultHandshakeHandler
import java.security.Principal
import java.util.*

class DmmaHandshakeHandler: DefaultHandshakeHandler() {
    override fun determineUser(
            request: ServerHttpRequest,
            wsHandler: WebSocketHandler,
            attributes: MutableMap<String, Any>
    ): Principal? {
        val principal = StompPrincipal(UUID.randomUUID())
        SessionHolder.sessions.add(principal)
        return principal
    }
    companion object val log = LoggerFactory.getLogger(DmmaHandshakeHandler::class.java)
}