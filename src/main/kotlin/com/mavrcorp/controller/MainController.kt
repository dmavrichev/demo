package com.mavrcorp.controller

import com.mavrcorp.domain.Greeting
import com.mavrcorp.domain.HelloMessage
import com.mavrcorp.domain.SessionHolder
import org.slf4j.LoggerFactory
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.messaging.simp.annotation.SendToUser
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Controller
import org.springframework.web.util.HtmlUtils
import java.security.Principal


@Controller
class MainController(
        private val template: SimpMessagingTemplate
) {
    @MessageMapping("/hello")
    @SendTo("/topic/greetings")
    fun receiveColor(message: HelloMessage): Greeting {
        return Greeting("Hello, " + HtmlUtils.htmlEscape(message.name) + "!")
    }

//    @Scheduled(fixedDelay = 10000)
    private fun bgColor() {
        val payload = Greeting("Public message")
        template.convertAndSend("/topic/greetings", payload)
        log.info("Send payload: $payload")
    }

    @MessageMapping("/message")
    @SendToUser("/queue/reply")
    fun processMessageFromClient(message: HelloMessage, principal: Principal): Greeting {
        log.info("Session: " + principal)
        return Greeting("Hello, ${HtmlUtils.htmlEscape(message.name)}!")
    }

    @Scheduled(fixedDelay = 8000)
    private fun bgColor2() {
        val payload = Greeting("Private message")
//        val session = SessionHolder.sessions.firstOrNull()?.name ?: ""
        SessionHolder.sessions.forEach {
            log.info("The element is $it")
            template.convertAndSendToUser(it.name,"/queue/reply", Greeting("Private message",it.name))
        }
    }

    companion object {
        private val log = LoggerFactory.getLogger(MainController::class.java)
    }
}