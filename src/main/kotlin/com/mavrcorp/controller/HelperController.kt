package com.mavrcorp.controller

import com.mavrcorp.domain.Greeting
import com.mavrcorp.domain.HelloMessage
import com.mavrcorp.domain.SessionHolder
import com.mavrcorp.domain.StompPrincipal
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.util.*

@Controller
@RequestMapping("/api")
@CrossOrigin("*")
class HelperController (private val template: SimpMessagingTemplate) {

    @GetMapping("/sessions")
    @ResponseBody
    fun getSessionsList():ResponseEntity<Set<StompPrincipal>> = ResponseEntity(SessionHolder.sessions, HttpStatus.OK)

    @DeleteMapping("/sessions/{sessionId}")
    @ResponseBody
    fun getSessionsList(@PathVariable("sessionId") sessionId:String ):ResponseEntity<StompPrincipal> {
        SessionHolder.sessions.remove(StompPrincipal(UUID.fromString(sessionId)))
        return ResponseEntity(StompPrincipal(UUID.fromString(sessionId)), HttpStatus.OK)
    }

    @PostMapping("/sessions/{sessionId}")
    @ResponseBody
    fun sendMessageToDefinedUser(@PathVariable("sessionId") sessionId:String, @RequestBody message: HelloMessage ):ResponseEntity<HelloMessage> {
        template.convertAndSendToUser(sessionId,"/queue/reply", message)
        return ResponseEntity(message, HttpStatus.OK)
    }

}