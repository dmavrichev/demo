package com.mavrcorp.domain

import java.security.Principal
import java.util.*

data class StompPrincipal(private val name: UUID): Principal {
    override fun getName() = name.toString()
}
