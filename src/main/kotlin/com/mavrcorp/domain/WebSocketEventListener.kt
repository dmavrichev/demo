package com.mavrcorp.domain

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.event.EventListener
import org.springframework.messaging.simp.SimpMessageSendingOperations
import org.springframework.messaging.simp.stomp.StompHeaderAccessor
import org.springframework.stereotype.Component
import org.springframework.web.socket.messaging.SessionConnectedEvent
import org.springframework.web.socket.messaging.SessionDisconnectEvent
import java.util.*

@Component
class WebSocketEventListener {
    @Autowired
    private val messagingTemplate: SimpMessageSendingOperations? = null

    @EventListener
    fun handleWebSocketConnectListener(event: SessionConnectedEvent) {
        val user = event.user
        logger.info("Received a new web socket connection, $event")
        logger.info("User, $user")
    }

    @EventListener
    fun handleWebSocketDisconnectListener(event: SessionDisconnectEvent) {
        val s = event.sessionId
        println("handleWebSocketDisconnectListener, $s")
        val headerAccessor = StompHeaderAccessor.wrap(event.message)
        val name = event.user.name
        println("handleWebSocketDisconnectListener, $headerAccessor")

        SessionHolder.sessions.remove(StompPrincipal(UUID.fromString(name)))
        println("User Disconnected, $name")
    }

    companion object {
        private val logger = LoggerFactory.getLogger(WebSocketEventListener::class.java)
    }
}
