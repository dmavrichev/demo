package com.mavrcorp.domain

data class ColorMessage(val colorString: String)