package com.mavrcorp.domain

object SessionHolder {
    val sessions = mutableSetOf<StompPrincipal>()
}