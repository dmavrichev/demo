import Vue from 'vue'
import App from './App.vue'

import Buefy from 'buefy'
import 'buefy/dist/buefy.css'
import axios from 'axios'

import SockJS from 'sockjs-client'
import Stomp from 'webstomp-client'

Vue.use(Buefy)

Vue.prototype.SockJS = SockJS
Vue.prototype.Stomp = Stomp
Vue.prototype.axios = axios.create({
  baseURL:"http://localhost:8080"
})

new Vue({
  el: '#app',
  render: h => h(App)
})
